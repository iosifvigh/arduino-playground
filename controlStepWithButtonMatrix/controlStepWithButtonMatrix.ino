/*4x4 Matrix Keypad connected to Arduino
This code prints the key pressed on the keypad to the serial port*/

#include <Servo.h>
#include <Keypad.h>

Servo myservo; 

const byte numRows= 4; //number of rows on the keypad
const byte numCols= 4; //number of columns on the keypad

//keymap defines the key pressed according to the row and columns just as appears on the keypad
char keymap[numRows][numCols]= 
{
{ '3', '7', 'B', 'F'}, 
{ '2', '6', 'A', 'E'}, 
{ '1', '5', '9', 'D'},
{ '0', '4', '8', 'C'}
};

//Code that shows the the keypad connections to the arduino terminals
byte rowPins[numRows] = {30, 32, 34, 36}; //Rows 0 to 3
byte colPins[numCols] = {38, 40, 42, 44}; //Columns 0 to 3

//initializes an instance of the Keypad class
Keypad myKeypad= Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

void setup()
{
  myservo.attach(9);
  Serial.begin(9600);
}

//If key is pressed, this key is stored in 'keypressed' variable
//If key is not equal to 'NO_KEY', then this key is printed out
//if count=17, then count is reset back to 0 (this means no key is pressed during the whole keypad scan process
void loop()
{
  char keyPressed = myKeypad.getKey();
  if (keyPressed != NO_KEY)
    {
    Serial.println(keyPressed);
    int keyValue = 0;
    switch (keyPressed) 
      {
      case '0': keyValue = 0; break;
      case '1': keyValue = 1; break;
      case '2': keyValue = 2; break;
      case '3': keyValue = 3; break;
      
      case '4': keyValue = 4; break;
      case '5': keyValue = 5; break;
      case '6': keyValue = 6; break;
      case '7': keyValue = 7; break;
      
      case '8': keyValue = 8; break;
      case '9': keyValue = 9; break;
      case 'A': keyValue = 10; break;
      case 'B': keyValue = 11; break;
      
      case 'C': keyValue = 12; break;
      case 'D': keyValue = 13; break;
      case 'E': keyValue = 14; break;
      case 'F': keyValue = 15; break;
      }
    int servoPosition = map(keyValue, 0, 16, 0, 180);
    myservo.write(servoPosition);              // tell servo to go to position in variable 'pos'
    delay(150);
    }
}
