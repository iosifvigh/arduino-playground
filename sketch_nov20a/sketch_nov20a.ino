/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
const int green1 = 3;
const int green2 = 4;
const int red    = 5;

const int switchPin = 8;
int delayTime = 1;
int rise = 1;
int sensorValue;
int mappedSensorValue;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  
   Serial.begin(9600);
   
   
  pinMode(green1, OUTPUT); 
  pinMode(green2, OUTPUT); 
  pinMode(red, OUTPUT); 
  
  pinMode(switchPin, INPUT);
 
}

// the loop routine runs over and over again forever:
void loop() {
  

  
  int switchState;
    
  switchState = digitalRead(switchPin);

  Serial.print("Switch = ");
  
  if(switchState == LOW) {
    Serial.print("low, ");
    
  sensorValue = analogRead(A0);
  mappedSensorValue = map(sensorValue, 0, 688, 0, 256);
  // print out the value you read:
  Serial.print("Potentiometru = ");
  Serial.print(sensorValue);
  Serial.print(", Valoare mapata = ");
  Serial.print(mappedSensorValue);

  Serial.print(" [");
  for(int i=0; i < 256; i+=2)  {
    Serial.print("_");
    if (i==mappedSensorValue || i==mappedSensorValue-1) {
    Serial.print("I");
    }
  }
  Serial.println("]");
  

  digitalWrite(red,    HIGH);   delay(sensorValue);
  digitalWrite(red,    LOW);    delay(sensorValue/2);
  }
  else{
    Serial.print("high, ");
  digitalWrite(green1, HIGH); delay(delayTime);
  digitalWrite(green1, LOW);  delay(delayTime/2);
  digitalWrite(green2, HIGH); delay(delayTime);
  digitalWrite(green2, LOW);  delay(delayTime/2);
  
  
  Serial.print("delayTime = ");
  Serial.print(delayTime);
  if(delayTime <110 && rise == 1)  {
    delayTime = delayTime + 4;
  }  else  {
  rise = 0;
  };
  
  if(delayTime >4 && rise == 0)  {
    delayTime = delayTime - 4;
  }  else  {
  rise = 1;
  };
  
  for(int i=0; i < delayTime; i++)  {
    Serial.print("-");
  }
  Serial.println(".");
  }
}
