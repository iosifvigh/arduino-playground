 
#include <Servo.h>
 
Servo myservo;  // create servo object to control a servo
                // a maximum of eight servo objects can be created
 
int servoPin = 9;
int servoPosition = 0;    // variable to store the servo position
int potentiometerPin = A0;    // select the input pin for the potentiometer
int potentiometerValue = 0;       // variable to store the value coming from the sensor
int potentiometerValueOld = 0;

void setup()
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  Serial.begin(9600);
}
 

 
void loop()
{
  potentiometerValueOld = analogRead(potentiometerPin);    // read the value from the sensor
  if ( abs(potentiometerValue - potentiometerValueOld) > 20 ) {
    potentiometerValue = potentiometerValueOld;
  } 
  servoPosition = map(potentiometerValue, 0, 1023, 0, 180);
  myservo.write(servoPosition);              // tell servo to go to position in variable 'pos'
  delay(15);
  
//  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees
//  {                                  // in steps of 1 degree
//    myservo.write(pos);              // tell servo to go to position in variable 'pos'
//    delay(15);                       // waits 15ms for the servo to reach the position
//  }
//  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees
//  {
//    myservo.write(pos);              // tell servo to go to position in variable 'pos'
//    delay(15);                       // waits 15ms for the servo to reach the position
//  }
}
