int YIn = A0;
int XIn = A1;
int digitalClick = 0;

int LF = 5; // LF
int LB = 6;
int RF = 10;
int RB = 11;

int THRESHOLD = 10;

void setup() {
  pinMode(digitalClick, INPUT);     // pin en modo salida
  pinMode(LF, OUTPUT);     // pin en modo salida
  pinMode(LB, OUTPUT);     // pin en modo salida
  pinMode(RF, OUTPUT);     // pin en modo salida
  pinMode(RB, OUTPUT);     // pin en modo salida
  Serial.begin(9600);
  analogReference(INTERNAL);
}

void loop() {

  int LBVal = 0;
  int RBVal = 0;
  int LFVal = 0;
  int RFVal = 0;

  // speed
  int x = map(analogRead(XIn), 0, 1023, 255, -255);
  int y = map(analogRead(YIn), 0, 1023, 255, -255);

  Serial.println(x);
  Serial.println(y);

  if (y > THRESHOLD) {
    LFVal = y;
    RFVal = y;

    if (x > THRESHOLD) {
      LFVal = LFVal - x;
    }
    if (x < -THRESHOLD) {
      RFVal = RFVal + x;
    }
  }
  if (y < -THRESHOLD) {
    LBVal = -y;
    RBVal = -y ;

    if (x > THRESHOLD) {
      LBVal = LBVal - x;
    }
    if (x < -THRESHOLD) {
      RBVal = RBVal + x;
    }

  }


  analogWrite(LF, LFVal);
  analogWrite(LB, LBVal);
  analogWrite(RF, RFVal);
  analogWrite(RB, RBVal);

  delay(20);
}

