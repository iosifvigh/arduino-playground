#include <Keypad.h>

// =============================== KEYPAD ===============================
const byte numRows= 4; //number of rows on the keypad
const byte numCols= 4; //number of columns on the keypad

//keymap defines the key pressed according to the row and columns just as appears on the keypad
char keymap[numRows][numCols]= 
{
  { '3', '7', 'B', 'F'}, 
  { '2', '6', 'A', 'E'}, 
  { '1', '5', '9', 'D'},
  { '0', '4', '8', 'C'}
};

//Code that shows the the keypad connections to the arduino terminals
byte rowPins[numRows] = {30, 32, 34, 36}; //Rows 0 to 3
byte colPins[numCols] = {38, 40, 42, 44}; //Columns 0 to 3

//initializes an instance of the Keypad class
Keypad myKeypad= Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

// =========================== L298N H-Bridge ===========================
int L298N_IN1 = 46;
int L298N_IN2 = 48;
int L298N_IN3 = 50;
int L298N_IN4 = 52;

const ACTION_STOP     = 0;  // 0000
const ACTION_FORWARD  = 10; // 1010
const ACTION_BACKWARD = 5;  // 0101
const ACTION_LEFT     = 6;  // 0110
const ACTION_RIGHT    = 9;  // 1001

// =============================== Void Setup ===============================
void setup() {
  pinMode(L298N_IN1, OUTPUT);
  pinMode(L298N_IN2, OUTPUT);
  pinMode(L298N_IN3, OUTPUT);
  pinMode(L298N_IN4, OUTPUT);
  Serial.begin(9600);
}

// =============================== Functions ===============================
void printBinary(int x) {
  Serial.print(bitRead(x, 0));
  Serial.print(bitRead(x, 1));
  Serial.print(bitRead(x, 2));
  Serial.println(bitRead(x, 3));
}

void writeMotorState(int x) {
  printBinary(x);

  if (bitRead(x, 0)) {
    digitalWrite(L298N_IN1, HIGH);
  } else {
    digitalWrite(L298N_IN1, LOW);
  };
  
  if (bitRead(x, 1)) {
    digitalWrite(L298N_IN2, HIGH);
  } else {
    digitalWrite(L298N_IN2, LOW);
  };
  
  if (bitRead(x, 2)) {
    digitalWrite(L298N_IN3, HIGH);
  } else {
    digitalWrite(L298N_IN3, LOW);
  };
  
  if (bitRead(x, 3)) {
    digitalWrite(L298N_IN4, HIGH);
  } else {
    digitalWrite(L298N_IN4, LOW);
  };
}


// =============================== Main Loop ===============================
void loop() {
char keyPressed = myKeypad.getKey();
  if (keyPressed != NO_KEY)
    {
    int keyValue = 0;
    switch (keyPressed) 
      {
      case '0': keyValue = 0; writeMotorState(0); break;
      case '1': keyValue = 1; writeMotorState(1); break;
      case '2': keyValue = 2; writeMotorState(2); break;
      case '3': keyValue = 3; writeMotorState(3); break;
      
      case '4': keyValue = 4; writeMotorState(4); break;
      case '5': keyValue = 5; writeMotorState(5); break;
      case '6': keyValue = 6; writeMotorState(6); break;
      case '7': keyValue = 7; writeMotorState(7); break;
      
      case '8': keyValue = 8;  writeMotorState(8); break;
      case '9': keyValue = 9;  writeMotorState(9); break;
      case 'A': keyValue = 10; writeMotorState(10); break;
      case 'B': keyValue = 11; writeMotorState(11); break;
      
      case 'C': keyValue = 12; writeMotorState(12); break;
      case 'D': keyValue = 13; writeMotorState(13); break;
      case 'E': keyValue = 14; writeMotorState(14); break;
      case 'F': keyValue = 15; writeMotorState(15); break;
      }
      delay(150);
    }
}
